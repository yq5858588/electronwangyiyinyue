const electron = require('electron');
const { app } = electron;
const { BrowserWindow } = electron;
const { autoUpdater } = electron;
const { dialog } = electron;

var path = require('path');
var cp = require('child_process');

let win;

function createWindow() {
  win = new BrowserWindow({
    width: 1200,
    height: 800,
    transparent: false,
    frame: true,
  });
  win.loadURL(`file://${__dirname}/html/index.html`);
  win.webContents.openDevTools(); // 打开 开发者工具
  win.setMenu(null);
  win.maximize();
  win.on('closed', () => {
    win = null;
  });
}

app.on('ready', () => {
  createWindow();
});

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (win === null) {
    createWindow();
  }
});
